#!/usr/bin/env bash
set -euo pipefail
set IFS='\n\t'


snowsql -c $SNOWSQL_CONNECTION \
    -q "SELECT SCHEMA_NAME FROM DATA_LAKE.INFORMATION_SCHEMA.SCHEMATA ORDER BY SCHEMA_NAME;" \
    -o output_format=plain \
    -o header=false \
    -o timing=false \
    -o friendly=false \
    -o output_file=schemas.txt &> /dev/null

# Remove schemas created by QLIK from the list of schemas
# Also ignore the INFORMATION_SCHEMA
# Remove trailing whitespace
cat schemas.txt | sed 's/ *$//g' | grep -v -e 'QLIK' -e 'INFORMATION_SCHEMA' > schemas.txt.temp
rm schemas.txt
mv schemas.txt.temp schemas.txt

while read schema
do
    mkdir -p $schema

    snowsql -c $SNOWSQL_CONNECTION \
        -q "SELECT TABLE_NAME FROM DATA_LAKE.INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$schema' ORDER BY TABLE_NAME;" -o output_format=plain \
        -o header=false \
        -o timing=false \
        -o friendly=false \
        -o output_file=$schema/tables.txt &> /dev/null

    sed -i 's/ *$//g' $schema/tables.txt

    while read table
    do
        snowsql -c $SNOWSQL_CONNECTION \
            -q "SELECT GET_DDL('TABLE', 'DATA_LAKE.$schema.$table', TRUE);" \
            -o output_format=plain \
            -o header=false \
            -o timing=false \
            -o friendly=false \
            -o output_file=$schema/$table.sql &> /dev/null

        sed -i 's/ *$//g' $schema/$table.sql
    done < $schema/tables.txt

done < schemas.txt
