# Introduction

This repository is related to
[DSE-3230](https://dunelmmcdev.atlassian.net/browse/DSE-3230). Its goal is to
expose the differences in data lake table schemas that exist right now between
the production Snowflake account and the sandbox account that is being deployed
via Terraform with this merge request.

# Working with this repo

The bash script `bin/get_ddl.sh` gets the DDL for each table in the `DATA_LAKE`
database of a Snowflake account and writes it to a file in the following
directory hierarchy:

- Snowflake account
    * Schema
        - Table

Here is an example of the directory structure built by this script, filtering
only the ASPS and ZETES schemas.

```
.
├── production
│   ├── ASPS
│   │   ├── STOCK.sql
│   │   └── tables.txt
│   ├── schemas.txt
│   └── ZETES
│       ├── tables.txt
│       └── ZETES.sql
└── sandbox
    ├── ASPS
    │   ├── STOCK.sql
    │   └── tables.txt
    ├── schemas.txt
    └── ZETES
        ├── tables.txt
        └── ZETES.sql

6 directories, 10 files
```

- `schemas.txt` contains a list of schemas for that account
- `tables.txt` contains a list of tables for that schema

To make it work, you need to install the `snowsql` Snowflake command line
utility and add named connections for each Snowflake account to your config
file.
[Here](https://docs.snowflake.com/en/user-guide/snowsql-start.html#using-named-connections)
is some relevant documentation.

You then need to pass the information of which Snowflake account to connect to
via the `SNOWSQL_CONNECTION` environment variable.

```
$ export SNOWSQL_CONNECTION=production
$ bin/get_ddl.sh
```
